package controllers;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import repositories.enums.EnumsRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class DataController extends Controller {

    private final EnumsRepository enumsRepository;

    @Inject
    public DataController(EnumsRepository enumsRepository) {
        this.enumsRepository = enumsRepository;
    }

    public CompletableFuture<Result> getPersistentData() {
        return enumsRepository.getAllEnums()
                .thenApply(Results::ok);
    }
}
