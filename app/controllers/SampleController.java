package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class SampleController extends Controller {

    public Result helloWorld() {
        return ok("`Hello, World!");
    }
}
