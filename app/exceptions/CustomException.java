package exceptions;

import play.mvc.Result;

import java.util.concurrent.CompletionException;

public abstract class CustomException extends Exception {

    public CustomException(String message) {
        super(message);
    }

    public abstract Result toResult();

    public static CompletionException completionException(CustomException exception) {
        return new CompletionException(exception);
    }
}
