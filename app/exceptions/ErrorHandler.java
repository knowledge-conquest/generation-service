package exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Environment;
import play.api.OptionalSourceMapper;
import play.http.JsonHttpErrorHandler;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

@Singleton
public class ErrorHandler extends JsonHttpErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

    @Inject
    public ErrorHandler(Environment environment, OptionalSourceMapper sourceMapper) {
        super(environment, sourceMapper);
    }

    @Override
    public CompletionStage<Result> onServerError(Http.RequestHeader request, Throwable exception) {
        LOGGER.error(exception.getMessage(), exception);

        Result result = checkForCustomException(exception);

        if (result == null) {
            Results.internalServerError("Internal Server Error");
        }

        return CompletableFuture.completedFuture(result);
    }

    private Result checkForCustomException(Throwable exception) {
        if (exception instanceof CustomException) {
            return ((CustomException) exception).toResult();
        }

        if (exception instanceof CompletionException) {
            return checkForCustomException(exception.getCause());
        }

        return null;
    }
}
