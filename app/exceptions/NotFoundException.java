package exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class NotFoundException extends CustomException {

    public NotFoundException() {
        super("Not Found");
    }

    @Override
    public Result toResult() {
        return Results.notFound();
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new NotFoundException());
    }
}
