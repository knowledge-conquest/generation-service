package models;

import lombok.Getter;

public enum CollectionType {
    WEAPON_TYPES("WeaponTypes"),
    EQUIPMENT_SLOTS("EquipmentSlots"),
    RESOURCE_TYPES("ResourceTypes"),
    ;

    @Getter
    private final String name;

    CollectionType(String name) {
        this.name = name;
    }

    public String getId(String key) {
        return getName() + "/" + key;
    }
}
