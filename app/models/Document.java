package models;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A single document of an ArangoDB Collection.
 */
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({

})
public abstract class Document {

    @Getter
    @Setter
    @JsonProperty("_key")
    private String key;

    @Getter
    @Setter
    @JsonProperty("_rev")
    private String revision;

    @JsonGetter("_id")
    public String getId() {
        return key != null ? getCollection().getName() + "/" + key : null;
    }

    @JsonGetter("type")
    public abstract DocumentType getDocumentType();

    @JsonIgnore
    public CollectionType getCollection() {
        return getDocumentType().getCollectionType();
    }

    /* Serialization helpers for API responses */

    public abstract DocumentResponse toResponse();

    public abstract class DocumentResponse {

        @Getter
        private String key;

        @Getter
        private String type;

        protected DocumentResponse() {
            key = Document.this.key;
            type = Document.this.getDocumentType().name();
        }
    }
}
