package models;

import lombok.Getter;

public enum DocumentType {
    WEAPON_TYPE(CollectionType.WEAPON_TYPES),
    EQUIPMENT_SLOT(CollectionType.EQUIPMENT_SLOTS),
    RESOURCE_TYPE(CollectionType.RESOURCE_TYPES),
    ;

    @Getter
    private final CollectionType collectionType;

    DocumentType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }
}