package models;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class Edge extends Document {

    /* Serialization helpers for API responses */

    @Override
    public abstract EdgeResponse toResponse();

    public abstract class EdgeResponse extends DocumentResponse {}
}
