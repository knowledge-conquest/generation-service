package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.ToString;
import models.serialization.ReferenceDeserializer;
import models.serialization.ReferenceSerializer;

/**
 * A Reference to an ArangoDB collection's document.
 * Contains the id of the document and the document itself if populated.
 * @param <T> The type of the document.
 */
@ToString(includeFieldNames = false)
@JsonSerialize(using = ReferenceSerializer.class)
@JsonDeserialize(using = ReferenceDeserializer.class)
public class Reference<T extends Document> {

    // The id of the document referenced.
    @Getter
    private String id;

    // The referenced document.
    @Getter
    private T document;

    /**
     * Creates an unpopulated document reference.
     * @param id the document id
     */
    public Reference(String id) {
        this.id = id;
    }

    /**
     * Create a populated document reference.
     * @param document the document
     */
    public Reference(T document) {
        this.id = document.getId();
        this.document = document;
    }

    /**
     * Checks whether a document is present or not.
     * @return True is populated.
     */
    public boolean isPopulated() {
        return document != null;
    }

    public void SetId(String id) {
        this.id = id;
        this.document = null;
    }

    public void setDocument(T document) {
        this.document = document;
        this.id = document.getId();
    }

    @JsonIgnore
    public String getKey() {
        return getId().split("/")[1];
    }
}
