package models.enums;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import models.Document;
import models.serialization.EnumDocumentResponseSerializer;

public abstract class EnumDocument extends Document {

    @Getter
    @Setter
    private String name;

    public EnumDocument(String name) {
        this.name = name;
    }

    @Override
    public EnumDocumentResponse toResponse() {
        return new EnumDocumentResponse();
    }

    @JsonSerialize(using = EnumDocumentResponseSerializer.class)
    public class EnumDocumentResponse extends DocumentResponse {

        @Getter
        private final String name;

        protected EnumDocumentResponse() {
            name = EnumDocument.this.getName();
        }
    }
}
