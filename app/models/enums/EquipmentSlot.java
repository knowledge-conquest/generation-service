package models.enums;

import models.DocumentType;

public class EquipmentSlot extends EnumDocument {

    public EquipmentSlot(String name) {
        super(name);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.EQUIPMENT_SLOT;
    }
}
