package models.enums;

import models.DocumentType;

public class ResourceType extends EnumDocument {

    public ResourceType(String name) {
        super(name);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RESOURCE_TYPE;
    }
}
