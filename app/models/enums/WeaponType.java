package models.enums;

import models.DocumentType;

public class WeaponType extends EnumDocument {

    public WeaponType(String name) {
        super(name);
    }

    @Override
    public DocumentType getDocumentType() {
        return null;
    }
}
