package models.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import models.enums.EnumDocument;

import java.io.IOException;

public class EnumDocumentResponseSerializer<T extends EnumDocument> extends JsonSerializer<T> {

    @Override
    public void serialize(T enumDocument, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeString(enumDocument.getName());
    }
}
