package models.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import models.Document;
import models.Reference;

import java.io.IOException;


/**
 * Custom serializer.
 * Used for serializing ArangoDB Reference objects.
 */
public class ReferenceSerializer<T extends Document> extends JsonSerializer<Reference<T>> {

    /**
     * Serializes the objects by adding a class value to the json that is used when de-serializing.
     * Also calls the serialize function from the Serializable interface on the object.
     * @param reference The reference object to serialize.
     * @param generator The json generator user.
     * @param provider The serializer provider.
     * @throws IOException Could be thrown by the json generator.
     */
    @Override
    public void serialize(Reference<T> reference, JsonGenerator generator, SerializerProvider provider) throws IOException {
        if (reference.isPopulated()) {
            generator.writeObject(reference.getDocument());

        } else {
            generator.writeString(reference.getId());
        }
    }
}
