package repositories;

import com.arangodb.async.ArangoCollectionAsync;
import com.arangodb.async.ArangoDatabaseAsync;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.entity.DocumentEntity;
import com.arangodb.model.DocumentCreateOptions;
import exceptions.CustomException;
import exceptions.NotFoundException;
import models.CollectionType;
import services.ArangoAsyncService;

import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Stream;

public abstract class Repository {

    private ArangoAsyncService arango;

    public Repository(ArangoAsyncService arango) {
        this.arango = arango;
    }

    public ArangoDatabaseAsync getDatabase() {
        return arango.getDatabase();
    }

    public ArangoCollectionAsync getCollection() {
        return getDatabase().collection(getCollectionType().getName());
    }

    public abstract CollectionType getCollectionType();

    public <T> CompletableFuture<String> create(T document) {
        return getCollection().insertDocument(document)
                .thenApply(DocumentCreateEntity::getId);
    }

    public <T> CompletableFuture<Stream<String>> create(Vector<T> documents) {
        return getCollection().insertDocuments(documents)
                .thenApply(result -> result.getDocuments().stream().map(DocumentEntity::getId));
    }

    public <T> CompletableFuture<T> createAndGet(T document) {
        return getCollection().insertDocument(document, new DocumentCreateOptions().returnNew(true))
                .thenApply(DocumentCreateEntity::getNew);
    }

    public <T> CompletableFuture<Stream<T>> createAndGet(Vector<T> documents) {
        return getCollection().insertDocuments(documents, new DocumentCreateOptions().returnNew(true))
                .thenApply(result -> result.getDocuments().stream().map(DocumentCreateEntity::getNew));
    }

    public <T> CompletableFuture<T> findByKey(String key, Class<T> clazz) {
        return getCollection().getDocument(key, clazz)
                .thenApply(result -> {
                    if (result == null) throw NotFoundException.completionException();
                    return result;
                });
    }
}
