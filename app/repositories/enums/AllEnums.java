package repositories.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import models.enums.EquipmentSlot;
import models.enums.ResourceType;
import models.enums.WeaponType;

import java.util.Vector;

@AllArgsConstructor
public class AllEnums {

    @Getter
    @Setter
    private Vector<WeaponType> weaponTypes;

    @Getter
    @Setter
    private Vector<EquipmentSlot> equipmentSlots;

    @Getter
    @Setter
    private Vector<ResourceType> resourceTypes;

    public AllEnums() {
        weaponTypes = new Vector<>();
        equipmentSlots = new Vector<>();
        resourceTypes = new Vector<>();
    }
}
