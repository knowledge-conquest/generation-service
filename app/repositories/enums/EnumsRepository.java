package repositories.enums;

import com.arangodb.util.MapBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import exceptions.NotFoundException;
import models.CollectionType;
import repositories.Repository;
import services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class EnumsRepository extends Repository {

    @Inject
    public EnumsRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return null;
    }

    public CompletableFuture<JsonNode> getAllEnums() {
        return super.getDatabase().query(
                "let weaponTypes = (FOR wt IN @@wts RETURN wt.name) " +
                        "let equipmentSlots = (FOR es IN @@ess RETURN es.name) " +
                        "let resourceTypes = (FOR rt IN @@rts RETURN rt.name) " +
                        "RETURN {weaponTypes, equipmentSlots, resourceTypes}",
                new MapBuilder()
                        .put("@wts", CollectionType.WEAPON_TYPES.getName())
                        .put("@ess", CollectionType.EQUIPMENT_SLOTS.getName())
                        .put("@rts", CollectionType.RESOURCE_TYPES.getName())
                        .get(),
                JsonNode.class
        ).thenApply(result -> {
            if (!result.hasNext()) throw NotFoundException.completionException();
            return result.next();
        });
    }
}
