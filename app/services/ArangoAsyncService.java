package services;

import com.arangodb.async.ArangoDBAsync;
import com.arangodb.async.ArangoDatabaseAsync;
import com.typesafe.config.Config;
import play.inject.ApplicationLifecycle;
import utils.VJack;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class ArangoAsyncService {

    private static final String DB_NAME_KEY = "arango.db.name";
    private static final String DB_HOST_KEY = "arango.db.host";
    private static final String DB_PORT_KEY = "arango.db.port";
    private static final String DB_USER_KEY = "arango.db.user";
    private static final String DB_PASSWORD_KEY = "arango.db.password";
    private static final String DB_TTL_KEY = "arango.db.ttl";

    private final String DB_NAME;
    private ArangoDBAsync arangoAsync;

    @Inject
    public ArangoAsyncService(ApplicationLifecycle lifecycle, Config config) {

        this.DB_NAME = config.getString(DB_NAME_KEY);
        String host = config.getString(DB_HOST_KEY);
        int port = config.getInt(DB_PORT_KEY);
        String user = config.getString(DB_USER_KEY) ;
        String password = config.getString(DB_PASSWORD_KEY);
        Long ttl = config.getLong(DB_TTL_KEY);
        if (ttl < 0) ttl = null;

        this.arangoAsync = new ArangoDBAsync.Builder()
                .host(host, port)
                .user(user)
                .password(password)
                .connectionTtl(ttl)
                .serializer(new VJack())
                .build();

        lifecycle.addStopHook(() -> {
            arangoAsync.shutdown();
            return CompletableFuture.completedFuture(null);
        });
    }

    public ArangoDatabaseAsync getDatabase() {
        return arangoAsync.db(DB_NAME);
    }
}
