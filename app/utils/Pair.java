package utils;

import lombok.Getter;
import lombok.Setter;

public class Pair<T, U> {

    @Getter @Setter
    private T a;

    @Getter @Setter
    private U b;

    public Pair(T a, U b) {
        this.a = a;
        this.b = b;
    }
}
