package utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Quantity<T> {

    @Getter @Setter
    private T object;

    @Getter @Setter
    private Long quantity;
}
