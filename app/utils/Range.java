package utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Range<T extends Number> {

    @Getter @Setter
    private T min;

    @Getter @Setter
    private T max;
}
