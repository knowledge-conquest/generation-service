package utils;

import com.arangodb.ArangoDBException;
import com.arangodb.jackson.dataformat.velocypack.VPackMapper;
import com.arangodb.util.ArangoSerialization;
import com.arangodb.util.ArangoSerializer;
import com.arangodb.velocypack.VPackParser;
import com.arangodb.velocypack.VPackSlice;
import com.arangodb.velocypack.exception.VPackException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.libs.Json;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * VJack is a custom version of VelocyJack.
 * There is a problem with VelocyJack and JsonTypeInfo/JsonSbTypes.
 * For the moment this simpler version is good enough but might be a bit slower.
 * FIXME: Use VelocyJack again if the problem is resolved.
 */
public class VJack implements ArangoSerialization {

    private ObjectMapper mapper;
    private VPackParser parser;

    public VJack() {
        mapper = new VPackMapper().setSerializationInclusion(JsonInclude.Include.ALWAYS);
        parser = new VPackParser.Builder().build();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T deserialize(VPackSlice vpack, Type type) throws ArangoDBException {

        try {
            final JsonNode node = mapper.readTree(Arrays.copyOfRange(
                    vpack.getBuffer(), vpack.getStart(), vpack.getStart() + vpack.getByteSize()
            ));

            return Json.fromJson(node, (Class<T>) type);

        } catch (final IOException e) {
            throw new ArangoDBException(e);
        }
    }

    @Override
    public VPackSlice serialize(Object entity) throws ArangoDBException {
        return serialize(entity, new ArangoSerializer.Options());
    }

    @Override
    public VPackSlice serialize(Object entity, Options options) throws ArangoDBException {

        try {
            return parser.fromJson(Json.toJson(entity).toString());
        } catch (final VPackException e) {
            throw new ArangoDBException(e);
        }
    }
}
