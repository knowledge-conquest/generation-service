name := "kc-generation-service"
organization := "com.jackeri"
maintainer := "captain@jackeri.com"

version := "0.1"

lazy val root = (project in file("."))
  .enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  "org.projectlombok" % "lombok" % "1.18.12" % "provided",
  "com.arangodb" % "arangodb-java-driver" % "6.6.3",
  "com.arangodb" % "jackson-dataformat-velocypack" % "0.1.5"
    exclude("org.slf4j", "slf4j-api")
    exclude("ch.qos.logback", "logback-classic"),
  "com.auth0" % "java-jwt" % "3.10.1",
  "org.mockito" % "mockito-core" % "3.3.3" % Test,
  "org.junit.jupiter" % "junit-jupiter-api" % "5.7.0-M1" % Test
)